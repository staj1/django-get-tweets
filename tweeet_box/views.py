from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect

from tweeet_box.models import Tweet
from .twitter import save_tweets_to_db


def tweet_page(request):
    save_tweets_to_db()
    tweets = Tweet.objects.order_by('-created_at')
    page = request.GET.get('page', 1)
    paginator = Paginator(tweets, 20)
    try:
        tweets = paginator.page(page)
    except PageNotAnInteger:
        tweets = paginator.page(1)
    except EmptyPage:
        tweets = paginator.page(paginator.num_pages)

    return render(request, 'tweet_page.html', {'tweets': tweets})


