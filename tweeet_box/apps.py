from django.apps import AppConfig


class TweeetBoxConfig(AppConfig):
    name = 'tweeet_box'
